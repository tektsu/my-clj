(ns my_clj.square
  (:require [quil.core :as q]))

(defn setup []
  (q/background 240)
  (q/stroke 0)

  (let [size 500
        offset 50
        center-x (/ (q/width) 2)
        center-y (/ (q/height) 2)
        top-left-x (- center-x (/ size 2))
        top-left-y (- center-y (/ size 2))]
    (q/fill 200 150 250 150)
    (q/rect top-left-x top-left-y size size)
    (q/rect (- top-left-x offset) (- top-left-y offset) size size)
    (q/rect (+ top-left-x offset) (+ top-left-y offset) size size)))

(defn -main [& args]
  (q/defsketch squares
    :title "Squares"
    :setup setup
    :size [800 800]))
