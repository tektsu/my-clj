(ns my_clj.sierpinski
  (:require [quil.core :as q]))

(defn midpoint
  [[x1 y1] [x2 y2]]
  [(/ (+ x1 x2) 2) (/ (+ y1 y2) 2)])

(def vertices
  (let [scale 350]
    [[0 (- scale)]
     [(* scale (q/cos (/ (* 5 Math/PI) 6))) (* scale (q/sin (/ (* 5 Math/PI) 6)))]
     [(* scale (q/cos (/ Math/PI 6))) (* scale (q/sin (/ Math/PI 6)))]]))

(def generate-points
  (let [seed [0 0]
        next #(midpoint % (rand-nth vertices))]
    (iterate next seed)))

(defn setup []
  (q/background 240)
  (q/stroke 100 0 0)
  (q/with-translation [(/ (q/width) 2) (/ (q/height) 2)]
    (doseq [[x y] (take 50000 generate-points)]
      (q/point x y))))

(defn -main [& args]
  (q/defsketch sierpinksi
    :title "Sierpinksi Triangle"
    :setup setup
    :size [800 800]
    :features [:keep-on-top]))
