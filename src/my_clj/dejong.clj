(ns my_clj.dejong
  (:require [quil.core :as q]))

(def wd 800)
(def ht 800)

(defn rand-dj-param []
  (- 3 (rand 6)))

(defn parse-argument [args pos]
  (if (<= (count args) pos)
    (rand-dj-param)
    (if (nth args pos)
      (Float/parseFloat (nth args pos))
      (rand-dj-param))))

(defn process-args [args]
  (map #(parse-argument args %) [0 1 2 3]))

(defn transform-fn [a b c d]
  (fn [[x y]]
    [(- (q/sin (* a y)) (q/cos (* b x)))
     (- (q/sin (* c x)) (q/cos (* d y)))]))

(defn setup [args]
  (println "a =" (nth args 0))
  (println "b =" (nth args 1))
  (println "c =" (nth args 2))
  (println "d =" (nth args 3))
  (q/smooth)
  (q/background 0)
  (q/stroke 0 0 200)
  (q/with-translation [(/ wd 2) (/ ht 2)]
    (let [points 200000
          scale 150
          generator (let [x (rand)
                          y (rand)
                          transform (apply transform-fn args)]
                      (iterate transform [x y]))]
      (doseq [[x y] (take points generator)]
        (q/point (* scale x) (* scale y))))))

(defn -main [& args]
  (q/defsketch dejong
    :title "de Jong"
    :setup (fn [] (setup (process-args args)))
    :size [wd ht]
    :features [:keep-on-top
               :exit-on-close]))
